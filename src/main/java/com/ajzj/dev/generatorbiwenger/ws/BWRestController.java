package com.ajzj.dev.generatorbiwenger.ws;

import com.ajzj.dev.generatorbiwenger.entity.DataEntityResponse;
import com.ajzj.dev.generatorbiwenger.service.BWService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 */
@RestController
public class BWRestController {

  @Autowired
  private BWService bwService;

  @GetMapping(value = "/v1/biwenger/rounds", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<DataEntityResponse> getDetailSeason(){

    return new ResponseEntity<>(bwService.getDetailSeason(), HttpStatus.OK);
  }

  @GetMapping(value = "/v1/biwenger/rounds/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<DataEntityResponse> getDetailSeason(
    @PathVariable(value = "id") String idRound){

    return new ResponseEntity<>(bwService.getDetailRound(idRound), HttpStatus.OK);
  }

  @GetMapping(value = "/v1/biwenger/report", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> generateReport(){

    bwService.generateReport();
    return new ResponseEntity<>(HttpStatus.OK);
  }
}
