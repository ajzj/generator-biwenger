package com.ajzj.dev.generatorbiwenger.model;

import lombok.Data;

/**
 *
 * @author Vector ITC Group
 */
@Data
public class Player {

    private String name;

    private String points;

    private String money;

}
