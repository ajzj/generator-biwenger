package com.ajzj.dev.generatorbiwenger.service;

import com.ajzj.dev.generatorbiwenger.entity.DataEntityResponse;

/**
 *
 * @author Vector ITC Group
 */
public interface BWServiceImpl {

  DataEntityResponse getRoundsLeague();

  DataEntityResponse getDetailSeason();

  DataEntityResponse getDetailRound(String id);

  void generateReport();
}
