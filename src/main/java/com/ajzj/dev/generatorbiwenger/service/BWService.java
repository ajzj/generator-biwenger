package com.ajzj.dev.generatorbiwenger.service;

import com.ajzj.dev.generatorbiwenger.entity.DataEntityResponse;
import com.ajzj.dev.generatorbiwenger.entity.RoundEntity;
import com.ajzj.dev.generatorbiwenger.entity.StandingEntity;
import com.ajzj.dev.generatorbiwenger.repository.BWRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 *
 * @author Vector ITC Group
 */
@Service
public class BWService implements BWServiceImpl {

  @Autowired
  public BWRepository repository;

  @Value("${biwenger.money-round.first}")
  private Double roundMoney1;

  @Value("${biwenger.money-round.second}")
  private Double roundMoney2;

  @Value("${biwenger.money-round.third}")
  private Double roundMoney3;

  @Value("${biwenger.money-final.first}")
  private Double finalMoney1;
  @Value("${biwenger.money-final.second}")
  private Double finalMoney2;
  @Value("${biwenger.money-final.third}")
  private Double finalMoney3;
  @Value("${biwenger.money-final.fourth}")
  private Double finalMoney4;
  @Value("${biwenger.money-final.fifth}")
  private Double finalMoney5;

  @Value("${biwenger.finalCup}")
  private Boolean finalCup;

  @Override
  public DataEntityResponse getRoundsLeague() {

    return repository.getRoundsLeague();
  }

  @Override
  public DataEntityResponse getDetailSeason() {

    return repository.getDetailSeason();
  }

  @Override
  public DataEntityResponse getDetailRound(String id) {

    return repository.getDetailRound(id);
  }

  @Override
  public void generateReport() {
    HashMap<String, DataEntityResponse> mapRounds = new HashMap<>();
    HashMap<String, Double> mapPlayerMoney = new HashMap<>();
    DataEntityResponse response;
    DataEntityResponse seasonEntity = getRoundsLeague();
    Double amount = 0d;
    for(RoundEntity round: seasonEntity.getData().getSeason().getRounds()){
      if(!round.getName().contains("aplazada")) {
//        System.out.format("Round: {}"+round.getName());
        response = getDetailRound(round.getId());
        mapRounds.put(round.getShort_name(), response);
        if (response.getData() != null && response.getData().getLeague() != null && response.getData().getLeague().getStandings() != null) {
          if (response.getData().getLeague().getStandings().size() == 20 && "finished".equals(round.getStatus())) {
            for (StandingEntity stand : response.getData().getLeague().getStandings()) {
              if (stand.getLineup() != null && stand.getLineup().getPosition() != null) {
                switch (stand.getLineup().getPosition()) {
                  case 1:
                    amount = mapPlayerMoney.get(stand.getName()) != null ? mapPlayerMoney.get(stand.getName()) : 0d;
                    mapPlayerMoney.put(stand.getName(), amount + roundMoney1);
                    break;
                  case 2:
                    amount = mapPlayerMoney.get(stand.getName()) != null ? mapPlayerMoney.get(stand.getName()) : 0d;
                    mapPlayerMoney.put(stand.getName(), amount + roundMoney2);
                    break;
                  case 3:
                    amount = mapPlayerMoney.get(stand.getName()) != null ? mapPlayerMoney.get(stand.getName()) : 0d;
                    mapPlayerMoney.put(stand.getName(), amount + roundMoney3);
                    break;
                  default:
                }
              }
            }
          }
        }
      }
    }
    seasonEntity = getDetailSeason();
    Double amountTotal = 0d;
    System.out.println("###########################################################################################");
    System.out.format("%5s%40s%10s%10s\n", "Pos.", "Nombre", "Puntos", "Pagar");
    System.out.println("###########################################################################################");
    for(StandingEntity stand: seasonEntity.getData().getLeague().getStandings()) {
      if(!finalCup) {
        amountTotal = printAmountPlayer(stand, mapPlayerMoney, amountTotal);
      }
      switch (stand.getPosition()) {
        case 1:
          amount = mapPlayerMoney.get(stand.getName()) != null ? mapPlayerMoney.get(stand.getName()) : 0d;
          mapPlayerMoney.put(stand.getName(), amount + finalMoney1);
          break;
        case 2:
          amount = mapPlayerMoney.get(stand.getName()) != null ? mapPlayerMoney.get(stand.getName()) : 0d;
          mapPlayerMoney.put(stand.getName(), amount + finalMoney2);
          break;
        case 3:
          amount = mapPlayerMoney.get(stand.getName()) != null ? mapPlayerMoney.get(stand.getName()) : 0d;
          mapPlayerMoney.put(stand.getName(), amount + finalMoney3);
          break;
        case 4:
          amount = mapPlayerMoney.get(stand.getName()) != null ? mapPlayerMoney.get(stand.getName()) : 0d;
          mapPlayerMoney.put(stand.getName(), amount + finalMoney4);
          break;
        case 5:
          amount = mapPlayerMoney.get(stand.getName()) != null ? mapPlayerMoney.get(stand.getName()) : 0d;
          mapPlayerMoney.put(stand.getName(), amount + finalMoney5);
          break;
        default:
          break;

      }
      if(finalCup) {
        amountTotal = printAmountPlayer(stand, mapPlayerMoney, amountTotal);
      }
    }
    System.out.println("###########################################################################################");
    System.out.println("Total: " + amountTotal);
  }


  private Double printAmountPlayer(StandingEntity stand, HashMap<String, Double> mapPlayerMoney, Double amountTotal){
    Double amount;
    amount = mapPlayerMoney.get(stand.getName()) != null
      ? mapPlayerMoney.get(stand.getName()) : 0d;
    amountTotal += amount;
    System.out.format("%5s%40s%10s%10s\n", stand.getPosition(), stand.getName(), stand.getPoints(), amount+" €");
    return amountTotal;
  }

}
