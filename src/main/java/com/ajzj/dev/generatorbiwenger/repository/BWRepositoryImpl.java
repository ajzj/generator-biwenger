package com.ajzj.dev.generatorbiwenger.repository;

import com.ajzj.dev.generatorbiwenger.entity.DataEntityResponse;

/**
 *
 * @author Vector ITC Group
 */
public interface BWRepositoryImpl {


  DataEntityResponse getRoundsLeague();

  DataEntityResponse getDetailSeason();

  DataEntityResponse getDetailRound(String id);
}
