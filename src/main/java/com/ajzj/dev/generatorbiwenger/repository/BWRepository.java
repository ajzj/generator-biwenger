package com.ajzj.dev.generatorbiwenger.repository;

import com.ajzj.dev.generatorbiwenger.constant.BWConstant;
import com.ajzj.dev.generatorbiwenger.entity.DataEntityResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDate;



/**
 *
 * @author Vector ITC Group
 */
@Slf4j
@Repository
public class BWRepository implements BWRepositoryImpl {

  @Autowired
  public RestTemplate restTemplate;

  @Value("${biwenger.token}")
  private String token;

  @Value("${biwenger.version}")
  private String version;

  /**
   *
   * @return
   */
  @Override
  public DataEntityResponse getRoundsLeague() {
    // Montamos la url
    UriComponentsBuilder url = UriComponentsBuilder
      .fromHttpUrl("https://cf.biwenger.com/api/v2/rounds/la-liga?score=1&lang=es&v="+version); //.queryParams(params);

    String urlRest = url.toUriString();

    //Montamos la llamada
    ResponseEntity<DataEntityResponse> salida;
    try {
      salida = restTemplate.getForEntity(urlRest, DataEntityResponse.class);
      log.info("Fin de llamada al RestTemplate: {} {} ", salida.getStatusCode(), LocalDate.now());
    } catch (Exception e){
      log.error("Error detail round {}", e);
      throw e;
    }

    return salida.getBody();
  }

  /**
   *
   * @return
   */
  @Override
  public DataEntityResponse getDetailSeason() {

    // Montamos la url
    UriComponentsBuilder url = UriComponentsBuilder
      .fromHttpUrl("https://biwenger.as.com/api/v2/rounds/league"); //.queryParams(params);
    String urlRest = url.toUriString();

    //Montamos la llamada
    ResponseEntity<DataEntityResponse> salida;
    try {
      salida = restTemplate.exchange(urlRest, HttpMethod.GET, new HttpEntity<>(createHeaders()), DataEntityResponse.class);
      log.info("Fin de llamada al RestTemplate: {} {} ", salida.getStatusCode(), LocalDate.now());
    } catch (Exception e){
      log.error("Error detail round {}", e);
      throw e;
    }

    return salida.getBody();
  }

  /**
   *
   * @param id
   * @return
   */
  @Override
  public DataEntityResponse getDetailRound(String id) {

    // Montamos la url
    UriComponentsBuilder url = UriComponentsBuilder
      .fromHttpUrl("https://biwenger.as.com/api/v2/rounds/league/"+id);
    String urlRest = url.toUriString();


    //Montamos la llamada
    ResponseEntity<DataEntityResponse> salida;
    try {
      salida = restTemplate.exchange(urlRest, HttpMethod.GET, new HttpEntity<>(createHeaders()), DataEntityResponse.class);
      log.info("Fin de llamada al RestTemplate: {} {} ", salida.getStatusCode(), LocalDate.now());
    } catch (Exception e){
      log.error("Error detail round by id , {}", e);
      throw e;
    }

    return salida.getBody();
  }


  private HttpHeaders createHeaders(){
    HttpHeaders headers = new HttpHeaders();
    headers.add("x-lang", "es");
    headers.add("x-league", BWConstant.LEAGUE);
    headers.add("x-user", BWConstant.USER);
    headers.add("x-version", version);
    headers.add("authorization", token);
    return headers;
  }
}
