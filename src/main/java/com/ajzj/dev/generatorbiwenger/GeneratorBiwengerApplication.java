package com.ajzj.dev.generatorbiwenger;

import com.ajzj.dev.generatorbiwenger.service.BWService;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * The Main application
 */
@EnableBatchProcessing
@SpringBootApplication
public class GeneratorBiwengerApplication {

	@Autowired
	private BWService service;

	public static void main(String[] args) {
		SpringApplication.run(GeneratorBiwengerApplication.class, args);

	}

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		// Do any additional configuration here
		return builder.build();
	}

	@Bean
	public void startBatch(){
		service.generateReport();
	}
}
