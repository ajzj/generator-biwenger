package com.ajzj.dev.generatorbiwenger.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 *
 * @author Vector ITC Group
 */
@Data
public class RoundEntity {

  private String id;
  private String name;
  @JsonProperty("short")
  private String short_name;
  private String status;
}
