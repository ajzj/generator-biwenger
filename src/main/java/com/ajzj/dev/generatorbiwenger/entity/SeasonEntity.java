package com.ajzj.dev.generatorbiwenger.entity;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Vector ITC Group
 */
@Data
public class SeasonEntity {

  private String id;
  private String name;
  private String slug;
  private List<RoundEntity> rounds = new ArrayList<RoundEntity>();
}
