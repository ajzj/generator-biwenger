package com.ajzj.dev.generatorbiwenger.entity;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Vector ITC Group
 */
@Data
public class LeagueEntity {

  private String id;
  private String name;
//  private CompetitionEntity competition;
  private List<StandingEntity> standings = new ArrayList<StandingEntity>();
}
