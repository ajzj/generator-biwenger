package com.ajzj.dev.generatorbiwenger.entity;

import lombok.Data;

/**
 *
 * @author Vector ITC Group
 */
@Data
public class DataEntity {

    private SeasonEntity season;

//    private CompetitionEntity competition;

    private LeagueEntity league;
}
