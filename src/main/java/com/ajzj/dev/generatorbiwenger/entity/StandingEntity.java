package com.ajzj.dev.generatorbiwenger.entity;

import lombok.Data;

/**
 *
 * @author Vector ITC Group
 */
@Data
public class StandingEntity {

  private String id;
  private String name;
  private int position;
  private String icon;
  private String points;
  private String teamValue;
  private LineUpEntity lineup;
}
