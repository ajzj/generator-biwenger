package com.ajzj.dev.generatorbiwenger.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 *
 * @author Vector ITC Group
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DataEntityResponse {

    private DataEntity data;
}
