package com.ajzj.dev.generatorbiwenger.entity;

import lombok.Data;

/**
 *
 * @author Vector ITC Group
 */
@Data
public class LineUpEntity {

  private String type;
  private Integer points;
  private Integer position;
  private String date;

}
