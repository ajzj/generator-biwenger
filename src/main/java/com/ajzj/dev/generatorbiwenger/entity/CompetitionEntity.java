package com.ajzj.dev.generatorbiwenger.entity;

import lombok.Data;

/**
 *
 * @author Vector ITC Group
 */
@Data
public class CompetitionEntity {

  private String id;
  private String slug;
}
